<?php
include 'dbcon.php';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
	<style type="text/css">
		h2{
			text-align: center;
			margin-top: 100px;
		}

		td,th{
			padding: 5px;
		}

		.id{
			background-color: black;
			color: white;
		}
	</style>
</head>
<body>

	<h2>Students Information</h2>

<center>
	<table border="1">
		<tr>
			<th class="id">Id</th>
			<th>Name</th>
			<th>Gender</th>
			<th>Age</th>
			<th>From</th>
			<th>To</th>
			<th colspan="3">Operation</th>
		</tr>

<?php
$i = 1;
$selectquery = "select * from user_info";
$query = $dbcon->prepare($selectquery);

$query->execute();

// var_dump($query->fetch()) or die();

while ($row = $query->fetch()) {

	?>

		<tr>
			<td class="id"><?php echo $i++; ?></td>
			<td><?php echo $row['name']; ?></td>
			<td><?php echo $row['gender'] ?></td>
			<td><?php echo $row['age'] ?></td>
			<td><?php echo $row['from_country'] ?></td>
			<td><?php echo $row['to_country'] ?></td>
			<td><a href="single_data_view.php?id=<?php echo $row['id'];?>">View</a></td>
			<td><a href="edit.php?id=<?php echo $row['id'];?>">Edit</a></td>
			<td>
				<form action="del.php" method="post">
					<input type="hidden" name="delete_id" value="<?php echo $row['id'];?>">

					<input type="submit" name="submit" value="Delete">
				</form>
			</td>
			
		</tr>
<?php
}
?>
	</table>
</center>
</body>
</html>