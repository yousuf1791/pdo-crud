<?php
include 'dbcon.php';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
	<style type="text/css">
		.view_data{
			margin: 10px;
			text-decoration: none;
			background-color: blue;
			color: white;
			padding: 5px;
		}
	</style>
</head>
<body>

	<?php
		$selectquery = "select * from user_info where id=:user_id";

		$query = $dbcon->prepare($selectquery);

		$query->bindParam('user_id', $_GET['id']);
		$query->execute();

		$row = $query->fetch();
		// var_dump($row);

		// echo $row['name'];
		// while ($row = $query->fetch()) {

		$f_c = $row['from_country'];

		$t_c = $row['to_country'];
	?>

	<form action="update.php" method="post">
		<input type="hidden" name="abcd" value="<?php echo$row['id']; ?>">
		Name: <input type="text" name="name" value="<?php echo$row['name']; ?>"><br><br>

		Gender: <input type="text" name="gender" value="<?php echo$row['gender']; ?>"><br><br>

		Age: <input type="text" name="age" value="<?php echo$row['age']; ?>"><br><br>

		Where are you from :  <select name="from_country">
					<option value="Afghanistan" <?php if($f_c == 'Afghanistan'){echo "selected";} ?>>Afghanistan</option>
					<option value="Albania" <?php if($f_c == 'Albania'){echo "selected";} ?>>Albania</option>	
					<option value="Algeria" <?php if($f_c == 'Algeria'){echo "selected";} ?>>Algeria</option>
					<option value="Andorra" <?php if($f_c == 'Andorra'){echo "selected";} ?>>Andorra</option>
					<option value="Argentina" <?php if($f_c == 'Argentina'){echo "selected";} ?>>Argentina</option>
					<option value="Australia" <?php if($f_c == 'Australia'){echo "selected";} ?>>Australia</option>
					<option value="Albania" <?php if($f_c == 'Albania'){echo "selected";} ?>>Albania</option>
					<option value="Bahrain" <?php if($f_c == 'Bahrain'){echo "selected";} ?>>Bahrain</option>
					<option value="Bangladesh" <?php if($f_c == 'Bangladesh'){echo "selected";} ?>>Bangladesh</option>
					<option value="Belgium" <?php if($f_c == 'Belgium'){echo "selected";} ?>>Belgium</option>
					<option value="Brazil" <?php if($f_c == 'Brazil'){echo "selected";} ?>>Brazil</option>
					<option value="Canada" <?php if($f_c == 'Canada'){echo "selected";} ?>>Canada</option>
					<option value="China" <?php if($f_c == 'China'){echo "selected";} ?>>China</option>
					<option value="Denmark" <?php if($f_c == 'Denmark'){echo "selected";} ?>>Denmark</option>
					<option value="Egypt" <?php if($f_c == 'Egypt'){echo "selected";} ?>>Egypt</option>
					<option value="France" <?php if($f_c == 'France'){echo "selected";} ?>>France</option>
					<option value="Germany" <?php if($f_c == 'Germany'){echo "selected";} ?>>Germany</option>
					<option value="Hungary" <?php if($f_c == 'Hungary'){echo "selected";} ?>>Hungary</option>
					<option value="India" <?php if($f_c == 'India'){echo "selected";} ?>>India</option>
					<option value="South Africa" <?php if($f_c == 'South Africa'){echo "selected";} ?>>South Africa</option>
					<option value="Saudi Arabia" <?php if($f_c == 'Saudi Arabia'){echo "selected";} ?>>Saudi Arabia</option>
				</select> <br><br>

		Where you want to travel :  <select name="to_country">
					<option value="Afghanistan" <?php if($t_c == 'Afghanistan'){echo "selected";} ?>>Afghanistan</option>
					<option value="Albania" <?php if($t_c == 'Albania'){echo "selected";} ?>>Albania</option>	
					<option value="Algeria" <?php if($t_c == 'Algeria'){echo "selected";} ?>>Algeria</option>
					<option value="Andorra" <?php if($t_c == 'Andorra'){echo "selected";} ?>>Andorra</option>
					<option value="Argentina" <?php if($t_c == 'Argentina'){echo "selected";} ?>>Argentina</option>
					<option value="Australia" <?php if($t_c == 'Australia'){echo "selected";} ?>>Australia</option>
					<option value="Albania" <?php if($t_c == 'Albania'){echo "selected";} ?>>Albania</option>
					<option value="Bahrain" <?php if($t_c == 'Bahrain'){echo "selected";} ?>>Bahrain</option>
					<option value="Bangladesh" <?php if($t_c == 'Bangladesh'){echo "selected";} ?>>Bangladesh</option>
					<option value="Belgium" <?php if($t_c == 'Belgium'){echo "selected";} ?>>Belgium</option>
					<option value="Brazil" <?php if($t_c == 'Brazil'){echo "selected";} ?>>Brazil</option>
					<option value="Canada" <?php if($t_c == 'Canada'){echo "selected";} ?>>Canada</option>
					<option value="China" <?php if($t_c == 'China'){echo "selected";} ?>>China</option>
					<option value="Denmark" <?php if($t_c == 'Denmark'){echo "selected";} ?>>Denmark</option>
					<option value="Egypt" <?php if($t_c == 'Egypt'){echo "selected";} ?>>Egypt</option>
					<option value="France" <?php if($t_c == 'France'){echo "selected";} ?>>France</option>
					<option value="Germany" <?php if($t_c == 'Germany'){echo "selected";} ?>>Germany</option>
					<option value="Hungary" <?php if($t_c == 'Hungary'){echo "selected";} ?>>Hungary</option>
					<option value="India" <?php if($t_c == 'India'){echo "selected";} ?>>India</option>
					<option value="South Africa" <?php if($t_c == 'South Africa'){echo "selected";} ?>>South Africa</option>
					<option value="Saudi Arabia" <?php if($t_c == 'Saudi Arabia'){echo "selected";} ?>>Saudi Arabia</option>
				</select> <br><br>

		<a class="view_data" href="display.php" target="_blank">View data</a>

		<input type="submit" name="submit" value="Update">

	</form>
<?php

?>
</body>
</html>

