<?php
include 'dbcon.php';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
	<style type="text/css">
		.view_data{
			margin: 10px;
			text-decoration: none;
			background-color: blue;
			color: white;
			padding: 5px;
		}
	</style>
</head>
<body>

	<form action="#" method="post">
		Name: <input type="text" name="name"><br><br>

		Gender: <input type="text" name="gender"><br><br>

		Age: <input type="text" name="age"><br><br>	

		Where are you from :  <select name="from_country">
					<option value="Afghanistan">Afghanistan</option>
					<option value="Albania">Albania</option>	
					<option value="Algeria">Algeria</option>
					<option value="Andorra">Andorra</option>
					<option value="Argentina">Argentina</option>
					<option value="Australia">Australia</option>
					<option value="Albania">Albania</option>
					<option value="Bahrain">Bahrain</option>
					<option value="Bangladesh">Bangladesh</option>
					<option value="Belgium">Belgium</option>
					<option value="Brazil">Brazil</option>
					<option value="Canada">Canada</option>
					<option value="China">China</option>
					<option value="Denmark">Denmark</option>
					<option value="Egypt">Egypt</option>
					<option value="France">France</option>
					<option value="Germany">Germany</option>
					<option value="Hungary">Hungary</option>
					<option value="India">India</option>
					<option value="South Africa">South Africa</option>
					<option value="Saudi Arabia">Saudi Arabia</option>
				</select> <br><br>

		Where you want to travel : <select name="to_country">
					<option value="Afghanistan">Afghanistan</option>
					<option value="Albania">Albania</option>	
					<option value="Algeria">Algeria</option>
					<option value="Andorra">Andorra</option>
					<option value="Argentina">Argentina</option>
					<option value="Australia">Australia</option>
					<option value="Albania">Albania</option>
					<option value="Bahrain">Bahrain</option>
					<option value="Bangladesh">Bangladesh</option>
					<option value="Belgium">Belgium</option>
					<option value="Brazil">Brazil</option>
					<option value="Canada">Canada</option>
					<option value="China">China</option>
					<option value="Denmark">Denmark</option>
					<option value="Egypt">Egypt</option>
					<option value="France">France</option>
					<option value="Germany">Germany</option>
					<option value="Hungary">Hungary</option>
					<option value="India">India</option>
					<option value="South Africa">South Africa</option>
					<option value="Saudi Arabia">Saudi Arabia</option>
				</select> <br><br>

		<a class="view_data" href="display.php" target="_blank">View data</a>

		<input type="submit" name="submit" value="insert">

	</form>

</body>
</html>

<?php

if (isset($_POST['submit'])) {

	try {
		// $server = "localhost";
		// $user = "root";
		// $pass = "";
		// $db = "phppdo";

		// $dbcon = new PDO("mysql:host=$server; dbname=$db", $user, $pass);

		$insertquery = "insert into user_info(name, gender, age, from_country, to_country)
						value(:n, :g, :a, :f_c, :t_c)";

		$result = $dbcon->prepare($insertquery);

		$result->bindParam('n', $_POST['name']);
		$result->bindParam('g', $_POST['gender']);
		$result->bindParam('a', $_POST['age']);
		$result->bindParam('f_c', $_POST['from_country']);
		$result->bindParam('t_c', $_POST['to_country']);

		$result->execute();

		if ($result) {
			?>
				<script type="text/javascript">
					alert("Data Inserted");
				</script>
			<?php
}

	} catch (PDOException $e) {
		echo "Error : " . $e->getMessage();
	}
	?>
		<!-- <script type="text/javascript">
			window.location.href="index.php";
		</script> -->
	<?php
}

?>