<?php 
include 'dbcon.php';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
	<style type="text/css">
		h2{
			text-align: center;
			margin-top: 100px;
		}

		.view_data{
			margin: 10px;
			text-decoration: none;
			background-color: blue;
			color: white;
			padding: 5px;
		}

		.create_new_user{
			margin: 10px;
			text-decoration: none;
			background-color: gray;
			color: white;
			padding: 5px;
		}
	</style>
</head>
<body>

	<h2>Students Information</h2>

<center>
	<table border="1">
		<tr>
			<th class="id">Id</th>
			<th>Name</th>
			<th>Gender</th>
			<th>Age</th>
			<th>From</th>
			<th>To</th>
			<th colspan="3">Operation</th>
		</tr>

<?php
$i = 1;
$selectquery = "select * from user_info where id=:user_id";
$query = $dbcon->prepare($selectquery);

$query->bindParam('user_id', $_GET['id']);

$query->execute();

// var_dump($query->fetch()) or die();

while ($row = $query->fetch()) {

	?>

		<tr>
			<td><?php echo $i++; ?></td>
			<td><?php echo $row['name']; ?></td>
			<td><?php echo $row['gender'] ?></td>
			<td><?php echo $row['age'] ?></td>
			<td><?php echo $row['from_country'] ?></td>
			<td><?php echo $row['to_country'] ?></td>
			<td><a href="edit.php?id=<?php echo $row['id'];?>">Edit</a></td>
			<td><a href="del.php" title="">Delete</a></td>
		</tr>
<?php
}
?>
	</table>
	<br>
	<br>
	<br>

	<a class="view_data" href="display.php" target="_blank">View data</a>

	<a class="create_new_user" href="index.php">Create New user</a>
</center>
</body>
</html>