<?php 
include 'dbcon.php';

if (isset($_POST['submit'])) {
	
	// $updatequery = "update studentstable set name = :user_name, age = :user_age, class = :user_class, gender = :user_gender where studentstable.'id' = :user_id";

	$updatequery = "UPDATE `user_info` SET `name` = :user_name, `gender` = :user_gender, `age` = :user_age, `from_country` = :f_c, `to_country` = :t_c WHERE `user_info`.`id` = :user_id;";

	// $updatequery = "UPDATE `user_info` SET `name` = :user_name, `gender` = :user_gender, `age` = :user_age, `from_country` = :f_c, `to_country` = :t_c WHERE `user_info`.`id` = :user_id;";

	$query = $dbcon->prepare($updatequery);

	$query->bindParam("user_id", $_POST['abcd']);
	$query->bindParam("user_name", $_POST['name']);
	$query->bindParam("user_gender", $_POST['gender']);
	$query->bindParam("user_age", $_POST['age']);
	$query->bindParam("f_c", $_POST['from_country']);
	$query->bindParam("t_c", $_POST['to_country']);
	

	$query->execute();

	if ($query) {
		?>
		<script type="text/javascript">
			alert("Data updated");
		</script>
		<?php
	}

	// header('location:display.php');
	?>
	<script type="text/javascript">
		window.location.href="display.php";
	</script>
	<?php

}

?>